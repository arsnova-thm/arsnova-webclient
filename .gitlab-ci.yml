variables:
  BUILD_DIR: dist
  NODE_VERSION: "12.16"
  YARN_CACHE_FOLDER: .yarn-cache

stages:
  - prepare
  - build
  - test
  - deploy

default:
  image: node:$NODE_VERSION-buster
  before_script:
    - export PATH="`pwd`/node_modules/.bin:$PATH"
    - date
  after_script:
    - date

.yarn_cache: &yarn_cache
  key: yarn-cache
  paths:
    - node_modules
    - $YARN_CACHE_FOLDER

yarn_populate_cache:
  stage: prepare
  rules:
    - if: '$CI_COMMIT_BRANCH != "master" || $CI_MERGE_REQUEST_ID'
      when: never
    - changes:
        - package.json
        - yarn.lock
      when: always
    - when: manual
  # allow_failure: Do not block pipeline if job is created but not run.
  allow_failure: true
  cache:
    <<: *yarn_cache
    policy: pull-push
  script:
    - echo Populating cache...
    - umask 0000
    - yarn --frozen-lockfile

yarn_install:
  stage: prepare
  artifacts:
    expire_in: 12 hours
    paths:
      - node_modules
  cache:
    <<: *yarn_cache
    policy: pull
  script:
    - umask 0000
    - yarn --frozen-lockfile

tslint:
  stage: test
  needs:
    - yarn_install
  allow_failure: false
  script:
    - tslint -p ./tsconfig.json -c ./tslint.json --project

unit_tests:
  stage: test
  needs:
    - yarn_install
  image: registry.gitlab.com/arsnova/devops/docker/nodejs_chrome:202003071658
  allow_failure: false
  variables:
    CHROME_BIN: /usr/bin/google-chrome
  script:
    - ng test --watch=false --browsers=ChromeHeadlessCustom

ngbuild:
  stage: build
  needs:
    - yarn_install
  allow_failure: false
  artifacts:
    paths:
      - "$BUILD_DIR"
  script:
    - ng build --prod

.docker_image: &docker_image
  stage: deploy
  dependencies:
    - ngbuild
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  # Disabled for now. See https://gitlab.com/gitlab-org/gitlab/issues/34756.
  #rules:
  #  - if: '$CI_COMMIT_REF_NAME == "master"'
  #    when: on_success
  #  - if: '$CI_COMMIT_REF_NAME == "latest" || $CI_COMMIT_REF_NAME =~ /^v[0-9]+/ || $CI_COMMIT_REF_NAME =~ /^[0-9]+\.[0-9]+$/'
  #    when: never
  #  - when: manual
  variables:
    KANIKO_CONTEXT: $CI_PROJECT_DIR
    KANIKO_DOCKERFILE: $CI_PROJECT_DIR/docker/Dockerfile
    KANIKO_DESTINATION: $CI_REGISTRY/$CI_PROJECT_PATH:$CI_COMMIT_REF_SLUG
  before_script:
    - date
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context "$KANIKO_CONTEXT" --dockerfile "$KANIKO_DOCKERFILE" --destination "$KANIKO_DESTINATION" --build-arg "ARSNOVA_BUILD_DIR=`echo $BUILD_DIR`"

# Temporary workaround until GitLab bug is fixed
docker_image:
  <<: *docker_image
  only:
    - master

docker_image_dev:
  <<: *docker_image
  except:
    - master
    - latest
    - /^v[0-9]+/
    - /^[0-9]+\.[0-9]+$/
  when: manual
