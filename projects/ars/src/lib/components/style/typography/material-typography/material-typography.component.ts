import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ars-style-typography-material',
  templateUrl: './material-typography.component.html',
  styleUrls: ['./material-typography.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MaterialTypographyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
